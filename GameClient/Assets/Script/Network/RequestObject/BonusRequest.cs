﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BonusRequest : BaseRequestObject
{

    public BonusRequest(string data)
    {
        actionName = "Bonus";
        dataPair = new Dictionary<string, string>();
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
