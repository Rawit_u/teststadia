﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainScene : BaseScene
{
    [SerializeField] private Text statusText;
    private string currentActionName;
	// Use this for initialization
	void Start ()
    {
	    base.Start();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnClickDrill()
    {
        Debug.Log("MainScene:OnClickDrill");
        NetworkController.GetInstance().Drill("Drill", serverResponseCallback);
        sendCommandUpdateText("Drill");
    }
    public void OnClickBonus()
    {
        Debug.Log("MainScene:OnClickBonus");
        sendCommandUpdateText("Bonus");
        NetworkController.GetInstance().Bonus("Bonus", serverResponseCallback);
    }
    public void OnClickQuickTrain()
    {
        Debug.Log("MainScene:OnClickQuickTrain");
        sendCommandUpdateText("QuickTrain");
        NetworkController.GetInstance().QuickTrain("QuickTrain", serverResponseCallback);
    }
    public void OnClickPhysio()
    {
        Debug.Log("MainScene:OnClickPhysio");
        sendCommandUpdateText("Physio");
        NetworkController.GetInstance().Physio("Physio", serverResponseCallback);
    }
    public void OnClickReport()
    {
        Debug.Log("MainScene:OnClickReport");
        sendCommandUpdateText("Report");
        NetworkController.GetInstance().Report("Report", serverResponseCallback);
    }
    public void OnClickPlayer()
    {
        Debug.Log("MainScene:OnClickPlayer");
        sendCommandUpdateText("Player");
        NetworkController.GetInstance().SelectPlayer("Player", serverResponseCallback);
    }
    public void OnClickTrain()
    {
        Debug.Log("MainScene:OnClickTrain");
        sendCommandUpdateText("Train");
        NetworkController.GetInstance().Train("Train", serverResponseCallback);
    }


    private void serverResponseCallback(bool isSuccess, NetworkResponseStatus status)
    {
        Debug.Log("MainScene:serverResponseCallback isSuccess: "+ isSuccess+ " Detail: "+status.Detail);
        updateNetworkStatusToText(isSuccess, status);
    }

    private void updateNetworkStatusToText(bool isSuccess, NetworkResponseStatus status)
    {
        string resultText = currentActionName;
        if (statusText == null)
            return;
        if (isSuccess)
        {
            resultText += " Success ";
        }
        else
        {
            resultText += " Fail ";
        }
        resultText += " " + status.Error;
        statusText.text = resultText;
    }

    //Update Text in Status bar to "Sending" after user press Button
    private void sendCommandUpdateText(string actionName)
    {
        if(statusText==null)
            return;
        statusText.text = actionName + " Sending...";
        currentActionName = actionName;
    }
}
