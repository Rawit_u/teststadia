﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestApp
{
    public class TestRequestClass
    {
        public const string SERVER_URL = "http://localhost:8100/api/values/";

        private static TestRequestClass instance;

        public TestRequestClass()
        {
            instance = this;
        }
        public static TestRequestClass GetInstance()
        {
            return instance;
        }

        public string GetDrillRequestUrl(string dataText)
        {
            return "Drill?datatext=" + dataText;

        }
        public string GetTrainingRequestUrl(bool isQuickTrain,string dataText)
        {
            return "Training?isQuickTrain={0}&datatext={1}".Replace("{0}",isQuickTrain.ToString()).Replace("{1}",dataText);

        }
        public string GetSelectPlayerRequestUrl(string dataText)
        {
            return "SelectPlayer?datatext=" + dataText;
        }
        public string GetBonusRequestUrl(string dataText)
        {
            return "Bonus?datatext=" + dataText;
        }
        public string GetPhysioRequestUrl(string dataText)
        {
            return "Physio?datatext=" + dataText;
        }
        public string GetReportRequestUrl(string dataText)
        {
            return "Report?datatext=" + dataText;
        }
    }
}
