﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace UnitTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRequestClass testRequest=new TestRequestClass();
            createMenu();
          
        }

        private static void createMenu()
        {
            Console.Clear();
            Console.WriteLine("Press 1: to test 'Drill' Request");
            Console.WriteLine("Press 2: to test 'Training' Request");
            Console.WriteLine("Press 3: to test 'SelectPlayer' Request");
            Console.WriteLine("Press 4: to test 'Bonus' Request");
            Console.WriteLine("Press 5: to test 'Physio' Request");
            Console.WriteLine("Press 6: to test 'Report' Request");
            Console.WriteLine(">>>");
            var readData=Console.ReadLine();
            int selectedMenu;
            bool dataisInt = int.TryParse(readData, out selectedMenu);
            bool isDataInRange = (selectedMenu <= 6 && (selectedMenu >= 1));

            if (!( dataisInt&&isDataInRange))
                createMenu();
            else
            {
                RunAsync(selectedMenu).Wait();
            }


        }



        static async Task RunAsync(int selectedMenu)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(TestRequestClass.SERVER_URL);
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string strParam = "";
                switch (selectedMenu)
                {
                    case 1:
                        strParam = TestRequestClass.GetInstance().GetDrillRequestUrl(randomString(5));
                        break;
                    case 2:
                        strParam = TestRequestClass.GetInstance().GetTrainingRequestUrl(randomBool(), randomString(5));
                        break;
                    case 3:
                        strParam = TestRequestClass.GetInstance().GetSelectPlayerRequestUrl(randomString(5));
                        break;
                    case 4:
                        strParam = TestRequestClass.GetInstance().GetBonusRequestUrl(randomString(5));
                        break;
                    case 5:
                        strParam = TestRequestClass.GetInstance().GetPhysioRequestUrl(randomString(5));
                        break;
                    case 6:
                        strParam = TestRequestClass.GetInstance().GetReportRequestUrl(randomString(5));
                        break;
                }

                HttpResponseMessage response = await client.GetAsync(strParam);
                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    var stream=await response.Content.ReadAsStreamAsync();
                    Console.WriteLine("Response Data "+ result);
                    Console.ReadLine();
                    createMenu();
                }

                return;
            }
        }


        private static Random random = new Random();
        private static string randomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static bool randomBool()
        {
            var data = DateTime.Now.Ticks%2;
            return Convert.ToBoolean(data);
        }
    }
}
