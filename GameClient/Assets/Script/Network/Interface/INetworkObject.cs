﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

public interface INetworkObject
{
    bool IsDone { get;}
    void SetupCallback(NetworkResponseStatus callback);
};
