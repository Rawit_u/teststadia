﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhysioRequest : BaseRequestObject
{

    public PhysioRequest(string data)
    {
        actionName = "Physio";
        dataPair = new Dictionary<string, string>();
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
