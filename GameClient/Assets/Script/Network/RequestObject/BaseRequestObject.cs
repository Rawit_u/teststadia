﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BaseRequestObject : INetworkObject
{
    public bool IsDone { get; protected set; }

    public string RequestURL
    {
        get
        {
            return requesParamUrl;
        }
    }

    public delegate void ResultCallback(bool success, NetworkResponseStatus detail);

    protected Dictionary<string, string> dataPair { get; set; }
    protected NetworkResponseStatus responseData;
    protected string requesParamUrl = "";
    protected string actionName = "";

    public BaseRequestObject()
    {
        IsDone = false;
    }

    //This fuction can be overridden in case you want a sub class to create difference request URL
    protected virtual void generateUrlString()
    {
        if (dataPair == null)
            requesParamUrl= "";

        requesParamUrl += "/" + actionName;
        int dataCount = 0;
        foreach (KeyValuePair<string, string> pair in dataPair)
        {
            if (dataCount == 0)
            {
                requesParamUrl += "?";
            }
            else if(dataCount <dataPair.Count)
            {
                requesParamUrl += "&";
            }
            requesParamUrl += pair.Key + "=" + pair.Value;
            dataCount++;
        }
    }

    

    public void SetupCallback(NetworkResponseStatus callback)
    {
        responseData = callback;
        IsDone = true;
    }
}
