﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReportRequest : BaseRequestObject
{

    public ReportRequest(string data)
    {
        actionName = "Report";
        dataPair = new Dictionary<string, string>();
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
