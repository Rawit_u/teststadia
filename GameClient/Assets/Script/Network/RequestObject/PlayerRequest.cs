﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerRequest : BaseRequestObject
{

    public PlayerRequest(string data)
    {
        actionName = "SelectPlayer";
        dataPair = new Dictionary<string, string>();
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
