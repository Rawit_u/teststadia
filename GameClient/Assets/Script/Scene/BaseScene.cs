﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class BaseScene : MonoBehaviour
{
    protected Dictionary<string, Button> CacheButtonObjects;
   
    // Use this for initialization
    protected virtual void Start ()
    {
        CacheButtonObjects = GetComponentsInChildren<Button>().ToDictionary(x => x.name);
    }

    // Update is called once per frame
    protected void Update ()
    {
	
	}
}
