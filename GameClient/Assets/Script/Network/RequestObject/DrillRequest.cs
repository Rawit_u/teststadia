﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrillRequest : BaseRequestObject
{

    public DrillRequest(string data)
    {
        actionName = "Drill";
        dataPair =new Dictionary<string, string>();
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
