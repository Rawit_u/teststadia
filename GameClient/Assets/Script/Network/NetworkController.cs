﻿using UnityEngine;
using System.Collections;
using UnityEngine.Experimental.Networking;

public class NetworkController : MonoBehaviour
{
    [SerializeField]
    private string productionServer;
    [SerializeField]
    private string stagingServer;

    [SerializeField]
    private bool isStaging;

    private const string suffix = "/api/values/";

    private string serverUrl = "";
    private static NetworkController instance;
    // Use this for initialization
    void Start ()
    {
        if (isStaging)
            serverUrl = stagingServer;
        else
            serverUrl = productionServer;

        serverUrl += suffix;
        instance = this;

    }

    public static NetworkController GetInstance()
    {
        return instance;
    }
    public void Drill(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new DrillRequest(text), callback);
    }
    public void Train(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new TrainingRequest(false,text), callback);
    }
    public void QuickTrain(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new TrainingRequest(true,text), callback);
    }
    public void SelectPlayer(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new PlayerRequest(text), callback);
    }
    public void Bonus(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new BonusRequest(text), callback);
    }
    public void Physio(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new PhysioRequest(text), callback);
    }
    public void Report(string text, BaseRequestObject.ResultCallback callback)
    {
        Request(new ReportRequest(text), callback);
    }


    //This will wrapup 'request' function because not all class in GameCode are base on MonoBehavior
    public void Request(BaseRequestObject requestObject, BaseRequestObject.ResultCallback callback)
    {
        string fullUrl = serverUrl + requestObject.RequestURL;
        StartCoroutine(request(fullUrl, callback));
    }

    private IEnumerator request(string url, BaseRequestObject.ResultCallback callback)
    {
        WWW wwwRequest = new WWW(url);

        yield return wwwRequest;
        
        NetworkResponseStatus status=new NetworkResponseStatus();
        status.Detail = wwwRequest.data;
        status.Error = wwwRequest.error;
        // check for errors
        if (wwwRequest.error == null)
            callback(true, status);
        else
            callback(false, status);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
