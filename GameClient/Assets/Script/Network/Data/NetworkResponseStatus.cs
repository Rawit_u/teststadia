﻿using UnityEngine;
using System.Collections;

public class NetworkResponseStatus
{
    public string Error { get; set; }
    public string Detail { get; set; }
}
