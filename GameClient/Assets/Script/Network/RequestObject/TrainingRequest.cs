﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrainingRequest : BaseRequestObject
{

    public TrainingRequest(bool isQuickTrain,string data)
    {
        actionName = "Training";
        dataPair = new Dictionary<string, string>();
        dataPair.Add("isQuickTrain", isQuickTrain.ToString());
        dataPair.Add("dataText", data);
        generateUrlString();
    }
}
